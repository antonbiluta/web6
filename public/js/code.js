document.addEventListener("DOMContentLoaded", function (event) {
    console.log("Dom loaded")
});
//Функция калькулятор
function raschitat() {
    kolvo = Number(document.getElementById('kol-vo').value);
    price = Number(document.getElementById('price').value);
    mesac = Number(document.getElementById('mesac').value);
    if (kolvo == "") {
        alert("Вы не указали кол-во товара");
    } else if (price == "") {
        alert("Вы не указали цену за 1 штуку");
    }else {
        proisvedenie = parseFloat(kolvo) * parseFloat(price);
        if(mesac ==""){
            summa = proisvedenie;
        } else {
            summa = proisvedenie / mesac;
        }
        if(Number.isNaN(proisvedenie) || Number.isNaN(summa) ){
            document.getElementById('proisvedenie').innerHTML = "Некорректный ввод данных.";
            document.getElementById('summa').innerHTML = "Используйте только цифры от 0 до 9.";
        }else{
            document.getElementById('proisvedenie').innerHTML = "Итоговая цена составит: " + proisvedenie + " рублей";
            document.getElementById('summa').innerHTML = "В месяц вам нужно платить: " + summa +" рублей, на протяжении " + mesac + " месяцев";
        }
    }
}

// Скрипт для подсветки обязательных полей красным
(function () {
    'use strict';
    window.addEventListener('load', function () {
        var forms = document.getElementsByClassName('needs-validation');
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

function count() {
  "use strict";
  var propertiesCost = 0;
  document.querySelectorAll("input[type=checkbox]").forEach(function (item) {
      if (item.checked) {
          propertiesCost += parseInt(item.value);
      }
  });
  var radiosCost = 0;
  document.querySelectorAll("input[type=radio]").forEach(function (item) {
      if (item.checked) {
          radiosCost += parseInt(item.value);
      }
  });

  var f1 = document.getElementsByName("field1");
  var f2 = document.getElementsByName("field2");
  var f3 = document.getElementsByName("field3");

  var r = document.getElementById("result");
  
  var x = f1[0].value * 150
          + f2[0].value * (80 + parseInt(propertiesCost))
          + f3[0].value * (110 + parseInt(radiosCost));
  r.innerHTML = (
      x.toString().match(/^[0-9]+$/) === null
      ? "Некорректный ввод"
      : "Итого: " + x + " руб."
  );

  return false; 
}

document.addEventListener("DOMContentLoaded", function () {
  "use strict";
  var list = document.getElementsByName("list");
  list[0].value = "1";
  document.getElementById("2nd").style.display = "none";
  document.getElementById("3rd").style.display = "none";
  document.getElementById("property").style.display = "none";
  document.getElementById("radios").style.display = "none";
  list[0].addEventListener("change", function (event) {
      var property = document.getElementById("property");
      var radios = document.getElementById("radios");
      var field1 = document.getElementById("1st");
      var field2 = document.getElementById("2nd");
      var field3 = document.getElementById("3rd");

      switch (event.target.value) {
      case "1":
          field1.style.display = "block";
          field2.style.display = "none";
          field3.style.display = "none";
          property.style.display = "none";
          radios.style.display = "none";
          break;
      case "2":
          field1.style.display = "none";
          field2.style.display = "block";
          field3.style.display = "none";
          property.style.display = "block";
          radios.style.display = "none";
          break;
      case "3":
          field1.style.display = "none";
          field2.style.display = "none";
          field3.style.display = "block";
          property.style.display = "none";
          radios.style.display = "block";
          break;
      }
  });
  [].forEach.call(document.getElementsByTagName("input"), function (item) {
      item.addEventListener("change", function () {
          count();
      });
  });
});

